# ReadyEvent and Logging

Now let's learn how we can use everything to our advantage.

JDA documents most important stuff on it's [docs](https://ci.dv8tion.net/job/JDA/javadoc/), so it will help you a lot with your journey on making a bot and its commands, plus some neat events.

> **NOTE:** Always put `@Override` before the function declaration for the function to work.

The reason we put `@Override` before the line of the function of the command is because the event is already declared, so you need to override that declaration with yours that has your code for the behavior to change.


What we're gonna learn on this is MORE useful events! (other than the `onMessageReceived` event) isn't that just exciting?

Now, we're gonna learn about the `onReady` event; that's when the bot is ready to receive and send events.

We do the same as we did when we wanted to add an event, make a new class and name it whatever you want. I'll be naming it "BotReadyEvent.java"

Then we use almost the same steps with the message listener.

We make it extend `ListenerAdapter`, then override the `onReady` event with the argument event of type `ReadyEvent`:

```java
public class BotReadyEvent extends ListenerAdapter {
    public void onReady(ReadyEvent event) {
        System.out.println("I'm ready, I'm ready, I'm ready!");
    }
}
```

This would be the `BotReadyEvent` class's code, now its to putting it on our JDA in our main class.

We go back to this portion:

```java
JDA jda = JDABuilder.createDefault(token)
          .addEventListener(new PingCmd())
          .setActivity(Activity.playing("with Finn & Jake"))
          .setStatus(OnlineStatus.DO_NOT_DISTURB)
          .build();

jda.awaitReady();
```

And just after the line where we defined JDA and before the line where we added the `PingCmd ListenerAdapter` in, we add the `BotReadyEvent` for the `ReadyEvent` to work, like this:

```java
    .addEventListener(new BotReadyEvent())
```

If you named your class any other name, you should change `BotReadyEvent` with it.

Of course, the `ReadyEvent event` argument we added has some properties!

You can use it to get the total number of guilds after the bot is ready, e.g.
(do this after you defined `ReadyEvent event` in your ready event)
`event.getGuildTotalCount();` would get the count of all available and unavailable guilds. To know how to get only the available or unavailable guilds I suggest you visit the docs [here](https://ci.dv8tion.net/job/JDA/javadoc/)

# Setting up Logback

Alright, so we're gonna set up a logger in our bot. 
A logger is better than normal printing because it doesn't take as much effort as `printlns`, and it logs any error (or everything, if you've set it to). 
JDA needs a `slf4j` logger (`slf4j` or ones based on it), and Logback is based on `slf4j`, and its one of the easiest to set up, you just need to put it in your `pom.xml` or `build.gradle`, then you have 1 more thing to do: make a `logback.xml` file and put it anywhere in your classpath, then add the configuration.

All of which can be found in the [Logback site that has docs, FAQs, code, etc](https://logback.qos.ch/index.html)

In the first step, you'd have to put in information about how you'd want your logging messages are formatted in `logback.xml`, here's the default thing in XML:

```xml
<configuration>

  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
    <!-- encoders are assigned the type
         ch.qos.logback.classic.encoder.PatternLayoutEncoder by default -->
    <encoder>
      <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
    </encoder>
  </appender>

  <root level="debug">
    <appender-ref ref="STDOUT" />
  </root>
</configuration>
```

You can change it however you want to make one that satisfies you, e.g. maybe remove the seconds, or remove the SSS so it doesn't show microseconds.

> **Note:** You also can make one called `logback.groovy` to use instead or make a testing version called `logback-test.xml`.

You don't need to do anything with the files other than this, since JDA uses it automatically by itself.

You can learn more about logging [here](http://logback.qos.ch/manual/configuration.html#auto_configuration)

Thats it for this one, see you!

---

Want to learn how to read and fix errors? Then go on to [**Reading Errors**](./reading-errors.html).
